# libUna
## ⚠ WARNING: Library still testing and is can be very unstable!
## General
**libUna** - is a powerfull minecraft launcher library based on LegacyLauncher *(aka TlauncherLegacy)*

### Code examples
Initialize minecraft launcher using `Launcher.initLauncher()`
```java
public class Main{
    public static void main(String[] args){
        Launcher liblauncher = Launcher.initLauncher();
    }
}
```
When we initialize the `Launcher` object, it automatically creates a configuration file located in `%appdata%/.launcher/liblauncher.properties`
There will also be other resources needed by the launcher and the game, such as the JDK.

After that we need refresh versions repositoryes
and create paid user
```java
public class Main{
    public static void main(String[] args){
        ...

        liblauncher.getVersionManager.refresh();

        User user = AccountManager.getPlainAuth().authorize("beengoo");
        liblauncher.getProfileManager().getAccountManager().getUserSet().add(user);
        liblauncher.getProfileManager().refresh();
    }
}
```

And finally run the game!

```java
public class Main{
    public static void main(String[] args){
        ...
        // For example run minecraft 1.19.2
        MinecraftLauncher minecraftLauncher = liblauncher.newMinecraftLauncher("1.19.2", false);
        MinecraftConfig minecraftConfig = new MinecraftConfig();
        minecraftConfig.xmx = "2048" // In megabytes
        minecraftConfig.accountName = "beengoo";
        minecraftConfig.accountType = Account.AccountType.PLAIN;
        minecraftLauncher.start(minecraftConfig);
    }
}
```