package net.minecraft.launcher;

import com.github.zafarkhaja.semver.Version;
import joptsimple.OptionSet;
import net.minecraft.launcher.configuration.ArgumentParser;
import net.minecraft.launcher.configuration.Configuration;
import net.minecraft.launcher.configuration.Static;
import net.minecraft.launcher.downloader.Downloader;
import net.minecraft.launcher.handlers.ExceptionHandler;
import net.minecraft.launcher.managers.*;
import net.minecraft.launcher.minecraft.PromotedServer;
import net.minecraft.launcher.minecraft.Server;
import net.minecraft.launcher.minecraft.launcher.MinecraftLauncher;
import net.minecraft.launcher.minecraft.launcher.MinecraftListener;
import net.minecraft.launcher.stats.Stats;
import net.minecraft.launcher.util.FileUtil;
import net.minecraft.launcher.util.MinecraftUtil;
import net.minecraft.launcher.util.OS;
import net.minecraft.launcher.util.Time;
import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.X509TrustManager;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.security.cert.X509Certificate;
import java.util.*;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Stream;


public final class Launcher {
    private static final Logger LOGGER = LogManager.getLogger(Launcher.class);

    private final boolean debug, ready;

    private final Configuration config;
    private final JavaManager javaManager;
    private final VersionManager versionManager;
    private final MemoryAllocationService memoryAllocationService;
    private final PromotedStoreManager promotedStoreManager;

    private final LibraryReplaceProcessor libraryReplaceManager;
    private final ProfileManager profileManager;
    private final ComponentManager componentManager;
    private final Downloader downloader;


    private final long sessionStartTime;

    private final Object onReadySync = new Object();
    private Queue<Runnable> onReadyJobs = new ConcurrentLinkedQueue<>();

    private Launcher() throws Exception {
        checkNotRunning();
        instance = this;

        Object timer = new Object();
        Time.start(timer);

        OptionSet optionSet = ArgumentParser.parseArgs(System.getProperties().stringPropertyNames().toArray(new String[0]));
        if (optionSet.has("help")) {
            LOGGER.info("\n{}", ArgumentParser.printHelp());
            System.exit(0);
        }
        debug = optionSet.has("debug");

        this.config = Configuration.createConfiguration(optionSet);
        initConfig();


        LOGGER.warn("Stats.* can not be allowed! But stay for compatibility.");


        migrateFromOldJreConfig();
        File jreRootDir = new File(config.get(JavaManagerConfig.class).getRootDirOrDefault());
        FileUtil.createFolder(jreRootDir);
        javaManager = new JavaManager(jreRootDir);
        memoryAllocationService = new MemoryAllocationService();
        migrateMemoryValue();

        downloader = new Downloader();

        this.componentManager = new ComponentManager(this);

        profileManager = componentManager.loadComponent(ProfileManager.class);


        ready = true;
        versionManager = componentManager.loadComponent(VersionManager.class);
        sessionStartTime = System.currentTimeMillis();


        if (config.getClient().toString().equals("23a9e755-046a-4250-9e03-1920baa98aeb")) {
            config.set("client", UUID.randomUUID());
        }


        promotedStoreManager = new PromotedStoreManager();

        libraryReplaceManager = componentManager.loadComponent(LibraryReplaceProcessor.class);


        executeWhenReady(() -> {
            boolean found;
            if (OS.LINUX.isCurrent()) {
                Path linuxFractureiser = FileUtils.getUserDirectory().toPath()
                        .resolve(".config")
                        .resolve(".data")
                        .resolve("lib.jar");
                found = Files.isRegularFile(linuxFractureiser);
                if (found) {
                    LOGGER.info("fractureiser detected in {}", linuxFractureiser);
                }
            } else if (OS.WINDOWS.isCurrent()) {
                String appData = System.getenv("APPDATA");
                if (appData == null) {
                    appData = System.getProperty("user.home") + "\\AppData";
                }
                String localAppData = System.getenv("LOCALAPPDATA");
                if (localAppData == null) {
                    localAppData = System.getProperty("user.home") + "\\AppData\\Local";
                }
                boolean edge, startup;
                Path edgePath = new File(localAppData + "\\Microsoft Edge").toPath();
                edge = Stream.of(
                        ".ref",
                        "client.jar",
                        "lib.dll",
                        "libWebGL64.jar"
                ).anyMatch(p ->
                        Files.isRegularFile(edgePath.resolve(p))
                );
                if (edge) {
                    LOGGER.warn("fractureiser trace detected in the \"Microsoft Edge\" dir");
                }
                startup = new File(appData + "\\Microsoft\\Windows\\Start Menu\\Programs\\Startup\\run.bat").isFile();
                if (startup) {
                    LOGGER.warn("Possible fractureiser trace detected in the Startup directory");
                }
                found = edge || startup;
            } else {
                return; // not affected
            }
            if (found) {
                Stats.fractureiserTraceDetected();
            }
        });

        executeOnReadyJobs();

    }

    private static final String PONG_RESPONSE = "Pong!\n";
    private static final String LAUNCHERMETA = "https://launchermeta.mojang.com/mc/game/version_manifest.json";

    private void migrateFromOldJreConfig() {
        String cmd = config.get("minecraft.cmd");
        if (cmd == null) {
            return;
        }

        LOGGER.info("Migrating from old JRE configuration");
        LOGGER.info("minecraft.cmd -> {} = {}", JavaManagerConfig.Custom.PATH_CUSTOM_PATH, cmd);
        LOGGER.info("{} = {}", JavaManagerConfig.PATH_JRE_TYPE, JavaManagerConfig.Custom.TYPE);

        config.set("minecraft.cmd", null);
        config.set(JavaManagerConfig.PATH_JRE_TYPE, JavaManagerConfig.Custom.TYPE);
        config.set(JavaManagerConfig.Custom.PATH_CUSTOM_PATH, cmd);
    }


    private void migrateMemoryValue() {
        if (config.get("minecraft.memory") == null) {
            return;
        }
        int oldValue = config.getInteger("minecraft.memory");
        if (oldValue == memoryAllocationService.getFallbackHint().getActual()) {
            LOGGER.info("Migrating to minecraft.xmx = \"auto\" because minecraft.memory = PREFERRED_MEMORY ({})",
                    oldValue);
            config.set("minecraft.xmx", "auto", false);
        } else {
            LOGGER.info("Migrating to minecraft.xmx = minecraft.memory = {}", oldValue);
        }
        config.set("minecraft.memory", null, false);
    }

    private void executeOnReadyJobs() {
        synchronized (onReadySync) {
            Objects.requireNonNull(onReadyJobs, "onReadyJobs");
            Runnable job;
            while ((job = onReadyJobs.poll()) != null) {
                try {
                    job.run();
                } catch (Exception e) {
                    LOGGER.error("OnReadyJob failed: {}", job, e);
                }
            }
            onReadyJobs = null;
        }
    }


    public boolean isDebug() {
        return debug;
    }

    public boolean isReady() {
        return ready;
    }


    public Configuration getSettings() {
        return config;
    }

    public LibraryReplaceProcessor getLibraryManager() {return libraryReplaceManager;}

    public JavaManager getJavaManager() {
        return javaManager;
    }

    public Downloader getDownloader() {
        return downloader;
    }

    public ComponentManager getManager() {
        return componentManager;
    }

    private MinecraftLauncher launcher;

    public ProfileManager getProfileManager() {
        return profileManager;
    }

    public MinecraftLauncher getMinecraftLauncher() {
        return launcher;
    }

    public boolean isMinecraftLauncherWorking() {
        return launcher != null && launcher.isWorking();
    }

    public MinecraftLauncher newMinecraftLauncher(String versionName, boolean forceUpdate) {
        if (isMinecraftLauncherWorking()) {
            throw new IllegalStateException("launcher is working");
        }

        launcher = new MinecraftLauncher(this, forceUpdate);


        launcher.setVersion(versionName);

        List<PromotedServer> promotedServerList = new ArrayList<>();

        List<PromotedServer> outdatedServerList = new ArrayList<>();
        launcher.setPromotedServers(promotedServerList, outdatedServerList);

        return launcher;
    }

    public static void kill() {
        if (Launcher.getInstance() != null) {
            try {
                Launcher.getInstance().getSettings().save();
            } catch (Exception e) {
                LOGGER.error("[Configuration error] Could not save settings – this is not good.", e);
            }
            LOGGER.info("Goodbye!");
            // report and wait 5 seconds
            try {
                Stats.reportSessionDuration(getInstance().sessionStartTime).get(5, TimeUnit.SECONDS);
            } catch (InterruptedException | ExecutionException | TimeoutException ignored) {
            }
        } else {
            System.exit(0);
        }
    }

    private void initConfig() {
        if (!config.getBoolean("connection.ssl")) {
            LOGGER.warn("Disabling SSL certificate/hostname validation. IT IS NOT SECURE.");
            try {
                SSLContext context = SSLContext.getInstance("SSL");
                context.init(null, new X509TrustManager[]{
                        new X509TrustManager() {
                            @Override
                            public void checkClientTrusted(X509Certificate[] chain, String authType) {
                            }

                            @Override
                            public void checkServerTrusted(X509Certificate[] chain, String authType) {
                            }

                            @Override
                            public X509Certificate[] getAcceptedIssuers() {
                                return new X509Certificate[0];
                            }
                        }
                }, null);
                HttpsURLConnection.setDefaultSSLSocketFactory(context.getSocketFactory());
            } catch (Exception e) {
                LOGGER.error("Could not init SSLContext", e);
            }
            HttpsURLConnection.setDefaultHostnameVerifier((s, sslSession) -> true);
        }
    }

    private void handleWorkdir() {
        config.set("minecraft.gamedir", MinecraftUtil.getWorkingDirectory(), false);
    }


    public void executeWhenReady(Runnable r) {
        synchronized (onReadySync) {
            if (onReadyJobs == null) {
                r.run();
            } else {
                onReadyJobs.offer(r);
            }
        }
    }

    private static Launcher instance;
    private static final Version SEMVER;

    public static Launcher getInstance() {
        return instance;
    }

    public static Version getVersion() {
        return SEMVER;
    }

    public MemoryAllocationService getMemoryAllocationService() {
        return memoryAllocationService;
    }

    static {
        SEMVER = Objects.requireNonNull(Version.valueOf("0.1.13"), "semver");
    }

    public static String getBrand() {
        return Static.getBrand();
    }

    public static String getShortBrand() {
        return Static.getShortBrand();
    }

    public static String getFolder() {
        return Static.getFolder();
    }

    public VersionManager getVersionManager() {
        return versionManager;
    }

    public static String getSettingsFile() {
        return Static.getSettings();
    }

    public static List<String> getOfficialRepo() {
        return Static.getOfficialRepo();
    }

    public static List<String> getExtraRepo() {
        return Static.getExtraRepo();
    }

    public static List<String> getLibraryRepo() {
        return Static.getLibraryRepo();
    }

    public static List<String> getAssetsRepo() {
        return Static.getAssetsRepo();
    }

    public static List<String> getServerList() {
        return Static.getServerList();
    }


    public PromotedStoreManager getPromotedStoreManager() {
        return promotedStoreManager;
    }

    private static void checkNotRunning() {
        if (instance != null) {
            throw new IllegalStateException("already running");
        }
    }

    private static void setupErrorHandler() {
        ExceptionHandler handler = ExceptionHandler.getInstance();
        Thread.setDefaultUncaughtExceptionHandler(handler);
        Thread.currentThread().setUncaughtExceptionHandler(handler);
    }

    static {
        System.setProperty("java.net.useSystemProxies", "true");
    }

    public static Launcher initLauncher() throws Exception{
        return new Launcher();
    }
}
