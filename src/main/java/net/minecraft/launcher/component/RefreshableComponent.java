package net.minecraft.launcher.component;

import net.minecraft.launcher.managers.ComponentManager;
import net.minecraft.launcher.util.async.AsyncThread;

public abstract class RefreshableComponent extends LauncherComponent {
    public RefreshableComponent(ComponentManager manager) {
        super(manager);
    }

    public boolean refreshComponent() {
        return refresh();
    }

    public void asyncRefresh() {
        AsyncThread.execute(this::refresh);
    }

    protected abstract boolean refresh();
}
