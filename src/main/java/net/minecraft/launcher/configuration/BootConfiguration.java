package net.minecraft.launcher.configuration;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import net.minecraft.launcher.minecraft.PromotedServer;
import net.minecraft.launcher.minecraft.PromotedServerDeserializer;

import java.util.*;

public final class BootConfiguration {
    private static final Logger LOGGER = LogManager.getLogger(BootConfiguration.class);

    private boolean stats, ely;
    private final Map<String, List<String>> repositories = new HashMap<>();
    private final Map<String, List<PromotedServer>> promotedServers = new HashMap<>();
    private final Map<String, List<PromotedServer>> outdatedPromotedServers = new HashMap<>();
    private final Map<String, String> feedback = new HashMap<>();
    private int allowNoticeDisable;

    public boolean isStatsAllowed() {
        return stats;
    }

    public boolean isElyAllowed() {
        return ely;
    }

    public Map<String, List<String>> getRepositories() {
        return repositories;
    }

    public Map<String, List<PromotedServer>> getPromotedServers() {
        return promotedServers;
    }

    public Map<String, List<PromotedServer>> getOutdatedPromotedServers() {
        return outdatedPromotedServers;
    }

    public Map<String, String> getFeedback() {
        return feedback;
    }

    public boolean isAllowNoticeDisable(UUID userId) {
        return (allowNoticeDisable > 0) && (userId.hashCode() % allowNoticeDisable == 0);
    }

    public int getAllowNoticeDisable() {
        return allowNoticeDisable;
    }
}
