package net.minecraft.launcher.configuration;

public interface Configurable {
    void load(AbstractConfiguration configuration);

    void save(AbstractConfiguration configuration);
}
