package net.minecraft.launcher.jna;

public class JNAException extends Exception {
    public JNAException(Throwable cause) {
        super(cause);
    }
}
