package net.minecraft.launcher.jre;

public interface JavaRuntime {
    String getName();

    String getPlatform();
}
