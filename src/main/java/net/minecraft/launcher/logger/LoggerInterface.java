package net.minecraft.launcher.logger;

public interface LoggerInterface {
    void print(String message);
}
