package net.minecraft.launcher.managed;

public interface ManagedListener<T> {
    void changedSet(ManagedSet<T> set);
}
