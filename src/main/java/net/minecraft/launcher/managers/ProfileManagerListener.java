package net.minecraft.launcher.managers;

import net.minecraft.launcher.minecraft.auth.AccountListener;

public interface ProfileManagerListener extends AccountListener {
    void onProfilesRefreshed(ProfileManager var1);

    void onProfileManagerChanged(ProfileManager var1);
}
