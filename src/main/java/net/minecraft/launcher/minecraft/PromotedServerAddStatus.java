package net.minecraft.launcher.minecraft;

public enum PromotedServerAddStatus {
    NONE, SUCCESS, EMPTY, DISABLED, ERROR
}
