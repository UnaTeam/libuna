package net.minecraft.launcher.minecraft.auth;

public interface AccountListener {
    void onAccountsRefreshed(AuthenticatorDatabase var1);
}
