package net.minecraft.launcher.minecraft.auth;

import net.minecraft.launcher.user.AuthException;
import net.minecraft.launcher.user.User;

import java.io.IOException;

public interface AuthExecutor<U extends User> {
    Account<U> pass() throws AuthException, IOException;
}
