package net.minecraft.launcher.minecraft.crash;

public interface Action {
    void execute() throws Exception;
}
