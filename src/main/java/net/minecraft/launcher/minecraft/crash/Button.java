package net.minecraft.launcher.minecraft.crash;

import com.google.gson.*;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import net.minecraft.launcher.util.StringUtil;
import net.minecraft.launcher.util.git.ITokenResolver;
import net.minecraft.launcher.util.git.TokenReplacingReader;

import java.awt.*;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class Button {
    private static final Logger LOGGER = LogManager.getLogger(Button.class);

    private final List<Action> actions = new ArrayList<>();

    private final String name;

    private String text;
    private boolean blockAfter, localizable = true, useGlobalPath = false;

    public Button(String name) {
        this.name = StringUtil.requireNotBlank(name, "name");
    }

    public final String getName() {
        return name;
    }

    public final String getText() {
        return text;
    }

    void setText(String text, Object... vars) {
        this.text = text;
    }

    public final List<Action> getActions() {
        return actions;
    }

    void setActions(List<Action> actions) {
        this.actions.clear();
        this.actions.addAll(actions.stream().filter(Objects::nonNull).collect(Collectors.toList()));
    }

    public final boolean isBlockAfter() {
        return blockAfter;
    }

    void setBlockAfter(boolean blockAfter) {
        this.blockAfter = blockAfter;
    }

    public final boolean isLocalizable() {
        return localizable;
    }

    void setLocalizable(boolean localizable, boolean useGlobalPath) {
        this.localizable = localizable;
        this.useGlobalPath = useGlobalPath;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this, ToStringStyle.SHORT_PREFIX_STYLE)
                .append("text", getText())
                .append("actions", getActions())
                .append("block", isBlockAfter())
                .append("loc", isLocalizable())
                .build();
    }

    public static class Deserializer implements JsonDeserializer<Button> {
        private final CrashManager manager;
        private final ITokenResolver resolver;

        public Deserializer(CrashManager manager, ITokenResolver resolver) {
            this.manager = Objects.requireNonNull(manager, "manager");
            this.resolver = resolver;
        }

        @Override
        public Button deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext context) throws JsonParseException {
            return deserialize(json, typeOfT, context);
        }
    }
}
