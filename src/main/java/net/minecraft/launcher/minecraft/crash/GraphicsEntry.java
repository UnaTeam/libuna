package net.minecraft.launcher.minecraft.crash;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import net.minecraft.launcher.util.OS;
import net.minecraft.launcher.util.sysinfo.GraphicsCard;
import net.minecraft.launcher.util.sysinfo.SystemInfo;

import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

public class GraphicsEntry extends PatternContainerEntry {
    private static final Logger LOGGER = LogManager.getLogger(GraphicsEntry.class);

    private final Pattern intelWin10BugJrePattern, intelWin10BugCardNamePattern, intelBugMinecraft1_10Pattern;
    private final PatternEntry amd, intel;

    public GraphicsEntry(CrashManager manager) {
        super(manager, "graphics");

        setAnyPatternMakesCapable(true);

        intelWin10BugJrePattern = manager.getVar("intel-win10-bug-jre-pattern") == null ? null : Pattern.compile("1\\.(?:[8-9]|[1-9][0-9]+)\\.[0-9](?:-.+|_(?!60)([1-9]?)(?:(1)[0-9]|[6-9])[0-9])");
        intelWin10BugCardNamePattern = manager.getVar("intel-win10-bug-card-pattern") == null ? Pattern.compile("Intel HD Graphics(?: [2-3]000)?") : Pattern.compile(manager.getVar("intel-win10-bug-card-pattern"));
        intelBugMinecraft1_10Pattern = manager.getVar("intel-bug-minecraft-1.10") == null ? Pattern.compile(".*1\\.(?:1[0-9]|[2-9][0-9])(?:\\.[\\d]+|)(?:-.+|)") : Pattern.compile(manager.getVar("intel-bug-minecraft-1.10"));

        addPattern("general", Pattern.compile("[\\s]*org\\.lwjgl\\.LWJGLException: Pixel format not accelerated"));
        addPattern("general", Pattern.compile("WGL: The driver does not appear to support OpenGL"));

        amd = addPattern("amd",
                Pattern.compile(manager.getVar("amd-pattern") == null ? "^#[ ]+C[ ]+\\[atio(?:gl|[0-9a-z]{2,})xx\\.dll\\+0x[0-9a-z]+]$" : manager.getVar("amd-pattern"))
        );

        intel = addPattern("intel",
                Pattern.compile(manager.getVar("intel-pattern") == null ? "^# C[ ]+\\[ig[0-9a-z]+icd(?:32|64)\\.dll\\+0x[0-9a-z]+]$" : manager.getVar("intel-pattern"))
        );
    }

    @Override
    protected boolean requiresSysInfo() {
        return true;
    }

    @Override
    protected boolean checkCapability(List<PatternEntry> capablePatterns) {
        if (!OS.WINDOWS.isCurrent()) {
            setPath("general-linux");
            return true;
        }

        setPath("general");

        if (capablePatterns.contains(amd)) {
            LOGGER.info("{} is relevant because the crash logs explicitly mention " +
                    "AMD graphics card drivers", getName());
            return setToUpdateDrivers("AMD");
        }

        if (capablePatterns.contains(intel)) {
            LOGGER.info("{} is relevant because the crash logs explicitly mention " +
                    "Intel graphics card drivers", getName());
            setToUpdateDrivers("Intel");

            if (intelBugMinecraft1_10Pattern.matcher(getManager().getVersion()).matches()) {
                LOGGER.info("We're currently running Minecraft 1.10+, but still having Intel HD graphics " +
                        "driver issue.");

                setPath("intel.1.10");
                addButton(getManager().getButton("open-settings"));
            }

            return true;
        }
        return false;
    }

    private boolean setToUpdateDrivers(String... manufacturers) {
        clearButtons();

        StringBuilder nameBuilder = new StringBuilder();
        for (String manufacturerName : manufacturers) {
            nameBuilder.append(", ").append(manufacturerName);

            String manufacturer = manufacturerName.toLowerCase(java.util.Locale.ROOT);
            newButton("driver-update", new VarUrlAction(manufacturer + "-driver-update", "https://wiki.llaun.ch/update:driver:" + manufacturer), manufacturerName);
        }
        setPath("update-driver", nameBuilder.substring(", ".length()));

        if (manufacturers.length == 1) {
            setImage("logo-" + manufacturers[0].toLowerCase(java.util.Locale.ROOT) + "@32");
        }

        return true;
    }

    private static Optional<GraphicsCard> getDisplayDevice(SystemInfo systemInfo, String name) {
        name = name.toLowerCase(java.util.Locale.ROOT);
        for (GraphicsCard card : systemInfo.getGraphicsCards()) {
            if (card.getName() == null) {
                continue;
            }
            if (card.getName().toLowerCase(java.util.Locale.ROOT).contains(name)) {
                return Optional.of(card);
            }
        }
        return Optional.empty();
    }

    private class VarUrlAction implements Action {
        private final String url;

        VarUrlAction(String varName, String fallbackUrl) {
            String url = getManager().getVar(varName);
            if (url == null) {
                url = fallbackUrl;
            }
            this.url = url;
        }

        @Override
        public void execute() {
            OS.openLink(url);
        }
    }
}
