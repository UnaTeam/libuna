package net.minecraft.launcher.minecraft.crash;

public interface RunnableAction extends Action {
    void execute();
}
