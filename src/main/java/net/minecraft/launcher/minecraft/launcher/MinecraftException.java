package net.minecraft.launcher.minecraft.launcher;


public class MinecraftException extends Exception {
    private final String langPath;

    MinecraftException(boolean send, String message, String langPath, Throwable cause, Object... langVars) {
        super(message, cause);
        if (langPath == null) {
            throw new NullPointerException("Lang path required!");
        } else {

            if (langVars == null) {
                langVars = new Object[0];
            }

            this.langPath = langPath;
        }
    }

    MinecraftException(boolean send, String message, String langPath, Throwable cause) {
        this(send, message, langPath, cause, new Object[0]);
    }

    MinecraftException(boolean send, String message, String langPath, Object... vars) {
        this(send, message, langPath, null, vars);
    }

    public String getLangPath() {
        return langPath;
    }

}
