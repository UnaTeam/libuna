package net.minecraft.launcher.pasta;

public interface PastaListener {
    void pasteUploading(Pasta pasta);

    void pasteDone(Pasta pasta);
}
