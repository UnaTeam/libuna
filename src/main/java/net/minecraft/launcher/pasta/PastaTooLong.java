package net.minecraft.launcher.pasta;

public class PastaTooLong extends PastaException {
    public PastaTooLong(long length) {
        super(String.valueOf(length));
    }
}
