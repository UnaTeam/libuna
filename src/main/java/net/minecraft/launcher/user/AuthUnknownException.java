package net.minecraft.launcher.user;

public class AuthUnknownException extends AuthException {
    public AuthUnknownException(Throwable t) {
        super(t, "unknown");
    }
}
