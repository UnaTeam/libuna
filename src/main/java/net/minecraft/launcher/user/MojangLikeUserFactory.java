package net.minecraft.launcher.user;

public interface MojangLikeUserFactory<M> {
    M createFromPayload(AuthlibUserPayload payload);
}
