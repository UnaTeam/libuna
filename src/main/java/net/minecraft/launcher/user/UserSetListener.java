package net.minecraft.launcher.user;

import net.minecraft.launcher.managed.ManagedListener;
import net.minecraft.launcher.managed.ManagedSet;

public interface UserSetListener extends ManagedListener<User> {
    void userSetChanged(UserSet set);

    default void changedSet(ManagedSet<User> set) {
        userSetChanged((UserSet) set);
    }
}
