package net.minecraft.launcher.user.minecraft.strategy.oareq.embed;

public interface URLCallback {
    void navigatedUrl(String url);
}
