package net.minecraft.launcher.user.minecraft.strategy.oareq.lcserv;

import net.minecraft.launcher.util.OS;

public class DefaultExternalBrowser implements ExternalBrowser {
    @Override
    public void openUrl(String url) {
        OS.openLink(url);
    }
}
