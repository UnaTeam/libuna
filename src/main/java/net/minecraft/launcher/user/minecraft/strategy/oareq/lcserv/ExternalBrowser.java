package net.minecraft.launcher.user.minecraft.strategy.oareq.lcserv;

public interface ExternalBrowser {
    void openUrl(String url);
}
