package net.minecraft.launcher.user.minecraft.strategy.pconv;

import net.minecraft.launcher.user.MinecraftUser;
import net.minecraft.launcher.user.minecraft.strategy.mcsauth.MinecraftServicesToken;
import net.minecraft.launcher.user.minecraft.strategy.oatoken.MicrosoftOAuthToken;
import net.minecraft.launcher.user.minecraft.strategy.preq.MinecraftOAuthProfile;

public class MinecraftProfileConverter {
    public MinecraftUser convertToMinecraftUser(MicrosoftOAuthToken microsoftToken,
                                                MinecraftServicesToken minecraftToken,
                                                MinecraftOAuthProfile profile)
            throws MinecraftProfileConversionException {
        try {
            return new MinecraftUser(
                    profile,
                    microsoftToken,
                    minecraftToken
            );
        } catch (RuntimeException rE) {
            throw new MinecraftProfileConversionException(rE);
        }
    }
}
