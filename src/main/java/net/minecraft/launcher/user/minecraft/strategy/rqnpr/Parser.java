package net.minecraft.launcher.user.minecraft.strategy.rqnpr;

import org.apache.logging.log4j.Logger;
import net.minecraft.launcher.exceptions.ParseException;

public interface Parser<V extends Validatable> {
    V parseResponse(Logger logger, String response) throws ParseException;
}
