package net.minecraft.launcher.util;

public class LazyInitException extends RuntimeException {
    LazyInitException(Throwable cause) {
        super(null, cause, true, false);
    }
}
