package net.minecraft.launcher.util.git;

public interface ITokenResolver {
    String resolveToken(String var1);
}
