package net.minecraft.launcher.util.sysinfo;

import java.util.concurrent.CompletableFuture;

public interface SystemInfoReporter {
    void queueReport();
    CompletableFuture<SystemInfo> getReport();
}
