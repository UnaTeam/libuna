package ua.beengoo.configuration;

import net.minecraft.launcher.configuration.Configuration;
import net.minecraft.launcher.managers.GPUManager;
import net.minecraft.launcher.managers.JavaManagerConfig;
import net.minecraft.launcher.user.User;
import net.minecraft.launcher.util.IntegerArray;

public class MinecraftConfig {
    public String accountName;
    public String accountType;
    public IntegerArray size = new IntegerArray(925, 530);
    public boolean fullscreen = false;
    public String jreType = JavaManagerConfig.Recommended.TYPE;
    public String gpu = GPUManager.GPU.DISCRETE.getName();
    public String xmx = "auto";
    public String onlaunch = Configuration.ActionOnLaunch.HIDE.toString();

    public boolean crash = true;

    public MinecraftConfig(User user) {
        this.accountName = user.getUsername();
        this.accountType = user.getType();
    }
    public MinecraftConfig(){}
    public void setAccountName(String accountName) {
        this.accountName = accountName;
    }

    public void setFullscreen(boolean fullscreen) {
        this.fullscreen = fullscreen;
    }

    public void setAccountType(String accountType) {
        this.accountType = accountType;
    }

    public void setCrash(boolean crash) {
        this.crash = crash;
    }

    public void setGpu(String gpu) {
        this.gpu = gpu;
    }

    public void setJreType(String jreType) {
        this.jreType = jreType;
    }

    public void setOnlaunch(String onlaunch) {
        this.onlaunch = onlaunch;
    }

    public void setSize(IntegerArray size) {
        this.size = size;
    }

    public void setXmx(String xmx) {
        this.xmx = xmx;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getAccountType() {
        return accountType;
    }

    public int[] getSize() {
        int[] value = new int[2];
        IntegerArray arr = this.size;
        value[0] = arr.get(0);
        value[1] = arr.get(1);
        return value;
    }

    public boolean isFullscreen() {
        return fullscreen;
    }

    public String getJreType() {
        return jreType;
    }

    public String getGpu() {
        return gpu;
    }

    public String getXmx() {
        return xmx;
    }

    public String getOnlaunch() {
        return onlaunch;
    }

    public boolean isCrash() {
        return crash;
    }
}
